package com.supanut.week13;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.Font;
import java.util.LinkedList;

import javax.swing.*;

public class QueueApp extends JFrame {
    JTextField txtName;
    JLabel lblQueueList, lblCurrent;
    JButton btnAddQueue, btnGetQueue, btnClearQueue;
    LinkedList<String> Queue;

    public QueueApp() {
        super("Queue App");
        Queue = new LinkedList();

        this.setSize(400, 300);
        txtName = new JTextField();
        txtName.setBounds(30, 10, 200, 20);
        txtName.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addQueue();
            }
        });

        btnAddQueue = new JButton("Add Queue");
        btnAddQueue.setBounds(250, 10, 100, 20);
        btnAddQueue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addQueue();
            }
        });

        btnGetQueue = new JButton("Get Queue");
        btnGetQueue.setBounds(250, 40, 100, 20);
        btnGetQueue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getQueue();
            }
        });

        btnClearQueue = new JButton("Clear Queue");
        btnClearQueue.setBounds(250, 70, 100, 20);
        btnClearQueue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearQueue();
            }
        });

        lblQueueList = new JLabel("Emply");
        lblQueueList.setBounds(30, 40, 200, 20);

        lblCurrent = new JLabel("???");
        lblCurrent.setHorizontalAlignment(JLabel.CENTER);
        lblCurrent.setFont(new Font("Serif", Font.PLAIN, 50));
        lblCurrent.setBounds(30, 70, 200, 50);

        this.add(txtName);
        this.add(btnAddQueue);
        this.add(btnClearQueue);
        this.add(btnGetQueue);
        this.add(lblQueueList);
        this.add(lblCurrent);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        showQueue();
        this.setVisible(true);
    }

    public void addQueue() {
        String name = txtName.getText();
        if (name.equals("")) {
            return;
        }
        Queue.add(name);
        txtName.setText("");
        showQueue();
    }

    public void clearQueue() {
        Queue.clear();
        lblCurrent.setText("?");
        txtName.setText("");
        showQueue();
    }

    public void getQueue() {
        if (Queue.isEmpty()) {
            lblCurrent.setText("?");
            return;
        }
        String name = Queue.remove();
        lblCurrent.setText(name);
        showQueue();
    }

    public void showQueue() {
        if (Queue.isEmpty()) {
            lblQueueList.setText("Empty");
        } else {
            lblQueueList.setText(Queue.toString());
        }
    }

    public static void main(String[] args) {
        QueueApp frame = new QueueApp();
    }
}
